var express = require('express');
var router = express.Router();
var userDal = require('../model/user_dal');


router.get('/new', function(req, res) {
    res.render('state/state_insert_form.ejs');
});

router.get('/state_insert', function(req, res){
    userDal.Insert(req.query.state_name, function(err, result){
        var response = {};
        if(err) {
            response.message = err.message;
        }
        else {
            response.message = 'Success!';
        }
        res.json(response);
    });
});


module.exports = router;